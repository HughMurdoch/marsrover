﻿using System;
using System.Text;
using MurdochMarsRover_v1.Engines;
using MurdochMarsRover_v1.Extensions;
using MurdochMarsRover_v1.Helpers;
using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Services
{
    public class MapService
    {
        public Map InitializeMap()
        {
            return SetGridDimensions();
        }

        private Map SetGridDimensions()
        {
            var gridDimensions = new[] { 0, 0 };
            do
            {
                Console.Write(
                    "Please enter the [X Y] integer values (seperated by a space) for the max number of columns and max number of rows: ");
                var response = Console.ReadLine()?.ToUpper().Split(' ');
                if (response == null || response.Length != 2) continue;
                int.TryParse(response[0], out gridDimensions[0]);
                int.TryParse(response[1], out gridDimensions[1]);
            } while (gridDimensions[0] < 1 || gridDimensions[1] < 1);

            var map = new Map { Columns = gridDimensions[0], Rows = gridDimensions[1] };
            return map;
        }

        public StringBuilder[] RenderMap(Map map, Rover rover)
        {
            MapBuilderEngine mbe = new MapBuilderEngine();
            return mbe.BuildMap(map, rover);
        }
    }
}