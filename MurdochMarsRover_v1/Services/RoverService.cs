﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MurdochMarsRover_v1.Enums;
using MurdochMarsRover_v1.Extensions;
using MurdochMarsRover_v1.Helpers;
using MurdochMarsRover_v1.Interfaces;
using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Services
{
    public class RoverService : IEngine
    {
        private Map _map;
        private MapService _mapService = new MapService();

        /// <summary>
        /// Initialize a Rover object and populate it's starting coordinates and orientation
        /// </summary>
        /// <param name="map">A map to ensure that the starting coordinates fall on the map.</param>
        /// <returns></returns>
        public Rover InitializeRover(Map map)
        {
            return SetRoverStartingPoint(map);
        }

        private Rover SetRoverStartingPoint(Map map)
        {
            _map = map;
            var roverLocation = new[] { 0, 0 };
            var roverOrientation = Orientation.N;
            var validOrientation = false;
            do
            {
                Console.Write(
                    $"Please enter the [X Y] integer values (seperated by a space) for the Rover location {Environment.NewLine}with a space followed by the Rover orientation [N, S, W, E] : ");

                var response = Console.ReadLine()?.ToUpper().Split(' ');
                if (response == null || response.Length != 3) continue;
                int.TryParse(response[0], out roverLocation[0]);
                int.TryParse(response[1], out roverLocation[1]);
                validOrientation = Enum.GetNames(typeof(Orientation)).Any(o => o.Equals(response[2]));
                if (validOrientation)
                    roverOrientation = (Orientation)Enum.Parse(typeof(Orientation), response[2]);
            } while (roverLocation[0] > _map.Columns || roverLocation[1] > _map.Rows || !(validOrientation));

            var rover = new Rover { Orientation = roverOrientation, Location = new Location { XPos = roverLocation[0], YPos = roverLocation[1] } };
            return rover;
        }

        /// <summary>
        /// Get a sequence of actions for the rover to execute and then execute them in order.
        /// </summary>
        /// <param name="rover">The rover object and it's starting settings</param>
        /// <param name="systemSettings">System settings to indicate if the output should be Verbose or include a ASCII GUI (version 1.2)</param>
        /// <returns>The updated rover object, so that if the user would like to run additional actions against it they can.</returns>
        public Rover GetAndRunRoverSequence(Rover rover, SystemSettings systemSettings)
        {
            try
            {
                Console.WriteLine(
                    "Please enter the run sequence of instructions [M, R or L] without any spaces:");

                var runSequence = Console.ReadLine()?
                    .Trim()
                    .Replace(" ", "")
                    .ToUpper()
                    .Where(c => "MRL".Contains(c));

                RunSequence(runSequence, rover, systemSettings);
                return rover;
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteLine($"An error has occured: {ex.Message}", ConsoleColor.Red);
                Console.WriteLine("Please press any key.");
                Console.ReadKey();
                return rover;
            }
        }

        private Rover RunSequence(IEnumerable<char> runSequence, Rover rover, SystemSettings systemSettings)
        {
            var sbOutput = new StringBuilder();
            if (_map == null)
                _map = new Map { Columns = 9, Rows = 9 };

            foreach (var action in runSequence)
            {
                switch (action)
                {
                    case 'M':
                        rover = MoveForward(rover);
                        break;
                    case 'L':
                        rover = TurnLeft(rover);
                        break;
                    case 'R':
                        rover = TurnRight(rover);
                        break;
                }

                if (systemSettings.GUIModeEnabled)
                {
                    Console.Clear();
                    var mapRender = _mapService.RenderMap(_map, rover);
                    ConsoleHelper.Write(mapRender[0].ToString(), ConsoleColor.DarkGray);
                    ConsoleHelper.Write(mapRender[1].ToString(), ConsoleColor.Green);
                    ConsoleHelper.Write(mapRender[2].ToString(), ConsoleColor.DarkGray);
                    Console.WriteLine();
                    Console.WriteLine();
                }

                if (systemSettings.VerboseModeEnabled)
                {
                    var output =
                        $"Action: {action} - Location: {rover.Location.XPos.ToString()} {rover.Location.YPos.ToString()} - Orientation: {rover.Orientation.OrientationArrow()}";
                    if (systemSettings.GUIModeEnabled)
                    {
                        sbOutput.Append(output);
                        sbOutput.Append(Environment.NewLine);
                    }

                    Console.WriteLine(output);
                }

                if (systemSettings.GUIModeEnabled)
                    Thread.Sleep(500);
            }
            Console.WriteLine();
            if (systemSettings.VerboseModeEnabled)
            {
                ConsoleHelper.WriteLine(sbOutput.ToString(), ConsoleColor.Yellow);
                Console.WriteLine();
            }

            Console.WriteLine("Final location and position:");
            Console.WriteLine($"{rover.Location.XPos.ToString()} {rover.Location.YPos.ToString()} {rover.Orientation.ToString()}");
            Console.WriteLine($"Location: {rover.Location.XPos.ToString()} {rover.Location.YPos.ToString()} - Orientation: {rover.Orientation.OrientationArrow()}");
            Console.WriteLine();
            Console.WriteLine("Please press any key.");
            Console.ReadKey();
            return rover;
        }

        /// <summary>
        /// Move forward method to update rover location depending on it's orientation
        /// </summary>
        /// <param name="rover">The rover object to update.</param>
        public Rover MoveForward(Rover rover)
        {
            switch (rover.Orientation)
            {
                case Orientation.N:
                    if (rover.Location.YPos < _map.Rows)
                        rover.Location.YPos++;
                    break;
                case Orientation.S:
                    if (rover.Location.YPos > 0)
                        rover.Location.YPos--;
                    break;
                case Orientation.W:
                    if (rover.Location.XPos > 0)
                        rover.Location.XPos--;
                    break;
                case Orientation.E:
                    if (rover.Location.XPos < _map.Columns)
                        rover.Location.XPos++;
                    break;
            }

            return rover;
        }

        /// <summary>
        /// Turns the rover left depending on it's orientation
        /// </summary>
        /// <param name="rover">The rover object to update.</param>
        public Rover TurnLeft(Rover rover)
        {
            switch (rover.Orientation)
            {
                case Orientation.N:
                    rover.Orientation = Orientation.W;
                    break;
                case Orientation.W:
                    rover.Orientation = Orientation.S;
                    break;
                case Orientation.S:
                    rover.Orientation = Orientation.E;
                    break;
                case Orientation.E:
                    rover.Orientation = Orientation.N;
                    break;
            }
            return rover;
        }

        /// <summary>
        /// Turns the rover right depending on it's orientation
        /// </summary>
        /// <param name="rover">The rover object to update.</param>
        public Rover TurnRight(Rover rover)
        {
            switch (rover.Orientation)
            {
                case Orientation.N:
                    rover.Orientation = Orientation.E;
                    break;
                case Orientation.E:
                    rover.Orientation = Orientation.S;
                    break;
                case Orientation.S:
                    rover.Orientation = Orientation.W;
                    break;
                case Orientation.W:
                    rover.Orientation = Orientation.N;
                    break;
            }
            return rover;
        }
    }
}