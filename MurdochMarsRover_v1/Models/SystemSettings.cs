﻿namespace MurdochMarsRover_v1.Models
{
    public class SystemSettings
    {
        public bool VerboseModeEnabled { get; set; }

        public bool GUIModeEnabled { get; set; }
        public SystemSettings()
        {
            VerboseModeEnabled = true;
            GUIModeEnabled = false;
        }
    }
}