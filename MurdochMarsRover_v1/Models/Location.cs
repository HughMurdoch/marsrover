﻿namespace MurdochMarsRover_v1.Models
{
    public class Location
    {
        public int XPos { get; set; }

        public int YPos { get; set; }
    }
}