﻿using System;

namespace MurdochMarsRover_v1.Models
{
    public class Map
    {
        public int Columns { get; set; }

        public int Rows { get; set; }
    }
}