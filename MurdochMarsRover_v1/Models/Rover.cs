﻿using System;
using System.Collections.Generic;
using System.Linq;
using MurdochMarsRover_v1.Enums;
using MurdochMarsRover_v1.Interfaces;

namespace MurdochMarsRover_v1.Models
{
    public class Rover
    {
        public Location Location { get; set; }

        public Orientation Orientation { get; set; }
    }
}