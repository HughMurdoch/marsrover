﻿namespace MurdochMarsRover_v1.Enums
{
    public enum Orientation
    {
        N, S, W, E
    }
}