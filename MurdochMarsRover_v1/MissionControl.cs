﻿using System;
using System.Collections.Generic;
using System.Linq;
using MurdochMarsRover_v1.Enums;
using MurdochMarsRover_v1.Extensions;
using MurdochMarsRover_v1.Helpers;
using MurdochMarsRover_v1.Interfaces;
using MurdochMarsRover_v1.Models;
using MurdochMarsRover_v1.Services;

namespace MurdochMarsRover_v1
{
    public class MissionControl
    {
        private Map _map;
        private Rover _rover;
        private const string _menuItems = "123458";
        private const int _maxGridSize = 9;
        private Services.MapService _mapService;
        private IEngine _roverService;
        private SystemSettings _systemSettings = new SystemSettings();
        private MapService _gridMap = new MapService();

        public MissionControl(IEngine roverService)
        {
            _roverService = roverService;
        }

        public void LaunchMissionControl()
        {
            _mapService = new Services.MapService();
            Console.WindowWidth = 140;
            Console.WindowHeight = 40;
            // Test initialization parameters
            //_map = new Map {Columns = 9, Rows = 9};
            //_rover = new Rover {Location = new Location {XPos = 4, YPos = 4}, Orientation = Orientation.N};
            InitializeMenu();
        }

        private void InitializeMenu()
        {
            var menuItem = 0;
            while (menuItem != 8)
            {
                menuItem = DisplayMenu();
                switch (menuItem)
                {
                    case 1:
                        Console.Clear();
                        _map = _mapService.InitializeMap();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        if (_map != null)
                        {
                            _rover = _roverService.InitializeRover(_map);
                            Console.Clear();
                        }
                        else
                            AlertFeedback("Please initialize map first");
                        break;
                    case 3:
                        _systemSettings.VerboseModeEnabled = !_systemSettings.VerboseModeEnabled;
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        if (!_systemSettings.GUIModeEnabled)
                        {
                            if (_map == null)
                                AlertFeedback("Please initialize map first");
                            else if (_map.Columns > _maxGridSize)
                                AlertFeedback($"Unfortunately the maximum number of columns for GUI mode is {_maxGridSize}. Please reinitialize the grid to use this mode.");
                            else if (_map.Rows > _maxGridSize)
                                AlertFeedback($"Unfortunately the maximum number of rows for GUI mode is {_maxGridSize}. Please reinitialize the grid to use this mode.");
                            else
                                _systemSettings.GUIModeEnabled = true;
                        }
                        else
                            _systemSettings.GUIModeEnabled = false;

                        break;
                    case 5:
                        Console.Clear();
                        if (_map != null && _rover != null)
                        {
                            _rover = _roverService.GetAndRunRoverSequence(_rover, _systemSettings);
                            Console.Clear();
                        }
                        else
                            AlertFeedback("Please initialize map and rover first");
                        break;
                }
            }
        }

        private int DisplayMenu()
        {
            RenderMenuItems();
            while (true)
            {
                var readKey = Console.ReadKey();
                if (readKey.KeyChar.ToString().Any(c => _menuItems.Contains(c)))
                    return int.Parse(readKey.KeyChar.ToString());
                Console.Clear();
                RenderMenuItems();
                Console.WriteLine();
                AlertFeedback("Please enter a valid integer value for the corresponding menu item.");
            }
        }

        private void RenderMenuItems()
        {
            WelcomeMessage();
            Console.WriteLine();
            Console.WriteLine(_map != null
                ? $"Map Grid: {_map.Columns.ToString()} {_map.Rows.ToString()}"
                : "Map Grid: [Not Initialized]");

            Console.WriteLine(_rover != null
                ? $"Rover Location: {_rover.Location.XPos} {_rover.Location.YPos} - Orientation {_rover.Orientation.OrientationArrow()}"
                : "Rover: [Not Initialized]");

            Console.WriteLine();
            Console.WriteLine("Murdoch Mars Rover Menu");
            Console.WriteLine("1. Initialize Grid Map");
            Console.WriteLine("2. Initialize Rover");
            Console.WriteLine($"3. Toggle Verbose Output [Enabled: {_systemSettings.VerboseModeEnabled.ToString() }]");
            Console.WriteLine($"4. Toggle GUI Map Output [Enabled: {_systemSettings.GUIModeEnabled.ToString() }]");
            Console.WriteLine("5. Get & Run Rover Sequence");
            Console.WriteLine();
            Console.WriteLine("8. Exit");
        }

        private void AlertFeedback(string alert)
        {
            ConsoleHelper.WriteLine(alert, ConsoleColor.Red);
            Console.WriteLine();
        }

        /// <summary>
        /// Method to return a welcome message basic implementation could be a simple string,
        /// more detailed could be an ASCII art message
        /// </summary>
        /// <returns></returns>
        private void WelcomeMessage()
        {
            ConsoleHelper.WriteLine(
                @"   _____                   .___            .__         _____                        __________                          
  /     \  __ _________  __| _/____   ____ |  |__     /     \ _____ _______  ______ \______   \ _______  __ ___________ 
 /  \ /  \|  |  \_  __ \/ __ |/  _ \_/ ___\|  |  \   /  \ /  \\__  \\_  __ \/  ___/  |       _//  _ \  \/ // __ \_  __ \
/    Y    \  |  /|  | \/ /_/ (  <_> )  \___|   Y  \ /    Y    \/ __ \|  | \/\___ \   |    |   (  <_> )   /\  ___/|  | \/
\____|__  /____/ |__|  \____ |\____/ \___  >___|  / \____|__  (____  /__|  /____  >  |____|_  /\____/ \_/  \___  >__|   
        \/                  \/           \/     \/          \/     \/           \/          \/                 \/       ",
                ConsoleColor.Blue);

        }
    }
}