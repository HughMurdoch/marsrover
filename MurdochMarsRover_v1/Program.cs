﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MurdochMarsRover_v1.Interfaces;
using MurdochMarsRover_v1.Services;
using Ninject;

namespace MurdochMarsRover_v1
{
    class Program
    {
        private bool _guiMode;
        static void Main(string[] args)
        {
            Ninject.IKernel kernal = new StandardKernel();
            kernal.Bind<IEngine>().To<RoverService>();
            var instance = kernal.Get<MissionControl>();
            instance.LaunchMissionControl();
        }
    }
}
