﻿using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Interfaces
{
    public interface IMove
    {
        Rover MoveForward(Rover rover);
    }
}