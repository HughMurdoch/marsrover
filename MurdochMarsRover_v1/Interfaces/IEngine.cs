﻿using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Interfaces
{
    public interface IEngine : IMove, ITurn
    {
        Rover InitializeRover(Map map);

        Rover GetAndRunRoverSequence(Rover rover, SystemSettings systemSettings);
    }
}