﻿using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Interfaces
{
    public interface ITurn
    {
        Rover TurnLeft(Rover rover);
        Rover TurnRight(Rover rover);
    }
}