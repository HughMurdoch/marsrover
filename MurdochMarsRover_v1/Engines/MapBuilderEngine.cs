﻿using System;
using System.Text;
using MurdochMarsRover_v1.Extensions;
using MurdochMarsRover_v1.Models;

namespace MurdochMarsRover_v1.Engines
{
    public class MapBuilderEngine
    {
        private const string _topLeft = "┌─────";
        private const string _topRight = "┐";
        private const string _bottomLeft = "└─────";
        private const string _bottomRight = "┘";
        private const string _leftT = "├";
        private const string _rightT = "─────┤";
        private const string _topT = "┬─────";
        private const string _bottomT = "┴─────";
        private const string _horizontal = "─────┼";
        private const string _vertical = "│";

        public StringBuilder[] BuildMap(Map map, Rover rover)
        {
            return GenerateMap(map, rover);
        }

        private static StringBuilder[] GenerateMap(Map map, Rover rover)
        {
            StringBuilder[] sb = { new StringBuilder(), new StringBuilder(), new StringBuilder() };
            var placeHolder = 0;

            for (var y = map.Rows; y >= 0; y--)
            {
                for (var x = 0; x <= map.Columns; x++)
                {
                    if (x == 0 && y == map.Rows)
                        sb[placeHolder].Append(_topLeft);
                    else if (y == map.Rows)
                        sb[placeHolder].Append(_topT);
                    else if (x == 0)
                        sb[placeHolder].Append(_leftT);
                    else
                        sb[placeHolder].Append(_horizontal);
                }

                sb[placeHolder].Append(y == map.Rows ? _topRight : _rightT);

                sb[placeHolder].Append(Environment.NewLine);

                for (var x = 0; x <= map.Columns; x++)
                    sb[placeHolder].Append($"{_vertical} {x},{y} ");

                sb[placeHolder].Append(_vertical);
                sb[placeHolder].Append(Environment.NewLine);

                for (var x = 0; x <= map.Columns; x++)
                    if (x == rover.Location.XPos && y == rover.Location.YPos)
                    {
                        sb[placeHolder].Append(_vertical);
                        placeHolder++;
                        sb[placeHolder].Append($"  {rover.Orientation.OrientationArrow()}  ");
                        placeHolder++;
                    }
                    else
                        sb[placeHolder].Append($"{_vertical}     ");

                sb[placeHolder].Append(_vertical);
                sb[placeHolder].Append(Environment.NewLine);
            }
            sb[placeHolder].Append(_bottomLeft);
            for (var x = 1; x <= map.Columns; x++)
                sb[placeHolder].Append(_bottomT);
            sb[placeHolder].Append(_bottomRight);
            return sb;
        }

    }
}