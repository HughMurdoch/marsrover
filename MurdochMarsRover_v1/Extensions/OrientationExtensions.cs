﻿using MurdochMarsRover_v1.Enums;

namespace MurdochMarsRover_v1.Extensions
{
    public static class OrientationExtensions
    {
        public static string OrientationArrow(this Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.N:
                    return "↑";
                case Orientation.S:
                    return "↓";
                case Orientation.W:
                    return "←";
                case Orientation.E:
                    return "→";
                default:
                    return orientation.ToString();
            }
        }
    }
}